
#ifndef LLVM_LIB_TARGET_FOXRISC_MCTARGETDESC_FOXRISCMCTARGETDESC_H
#define LLVM_LIB_TARGET_FOXRISC_MCTARGETDESC_FOXRISCMCTARGETDESC_H

#include "llvm/Support/DataTypes.h"

namespace llvm {
class Target;
class Triple;

extern Target ThefoxRISCTarget;

} // End llvm namespace

// Defines symbolic names for foxRISC registers.  This defines a mapping from register name to register number.
#define GET_REGINFO_ENUM
#include "foxRISCGenRegisterInfo.inc"

// Defines symbolic names for the foxRISC instructions.
#define GET_INSTRINFO_ENUM
#include "foxRISCGenInstrInfo.inc"

#define GET_SUBTARGETINFO_ENUM
#include "foxRISCGenSubtargetInfo.inc"

#endif
