#ifndef LLVM_LIB_TARGET_FOXRISC_FOXRISC_H
#define LLVM_LIB_TARGET_FOXRISC_FOXRISC_H

#include "MCTargetDesc/foxRISCMCTargetDesc.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Target/TargetMachine.h"

namespace llvm {

  class FunctionPass;
  class foxRISCTargetMachine;
  class MCInst;
  class MachineInstr;
  // class formatted_raw_ostream;
  // class AsmPrinter;

  namespace foxRISC {
    // foxRISC specific condition codes
    enum CondCodes {
      COND_E      = 3,    // equal
      COND_NE     = 2,    // not equal
      COND_GE     = 4,    // greater or equal
      COND_LE     = 5     // less or equal
    };

    inline static const char *foxRISCCondCodeToString(foxRISC::CondCodes CC) {
      switch (CC) {
      case foxRISC::COND_E:   return "e";
      case foxRISC::COND_NE:  return "ne";
      case foxRISC::COND_GE:  return "ge";
      case foxRISC::COND_LE:  return "le";
      }
      llvm_unreachable("Invalid cond code");
    }
  }

} // end namespace llvm;

#endif
