
class ImmAsmOperand<string prefix, int width, string suffix> : AsmOperandClass {
  let Name = prefix # "Imm" # width # suffix;
  let RenderMethod = "addImmOperands";
  let DiagnosticType = !strconcat("Invalid", Name);
}

class SImmAsmOperand<int width, string suffix = "">
    : ImmAsmOperand<"S", width, suffix> {
}

class UImmAsmOperand<int width, string suffix = "">
    : ImmAsmOperand<"U", width, suffix> {
}

def simm15 : Operand<XLenVT>, ImmLeaf<XLenVT, [{return isInt<15>(Imm);}]> {
  let ParserMatchClass = SImmAsmOperand<15>;
  let EncoderMethod = "getImmOpValue";
  let DecoderMethod = "decodeSImmOperand<15>";
  let MCOperandPredicate = [{
    int64_t Imm;
    if (MCOp.evaluateAsConstantImm(Imm))
      return isInt<15>(Imm);
    return MCOp.isBareSymbolRef();
  }];
  let OperandType = "OPERAND_SIMM15";
  let OperandNamespace = "RISCVOp";
}

def simm32     : ImmLeaf<XLenVT, [{return isInt<32>(Imm);}]>;

//===----------------------------------------------------------------------===//
// include instruction formats                                                //
//===----------------------------------------------------------------------===//

include "foxRISCInstrFormats.td"

//===----------------------------------------------------------------------===//
// defining instruction class templates                                       //
//===----------------------------------------------------------------------===//

// Branch commands

let hasSideEffects = 0, mayLoad = 0, mayStore = 0 in
class branch_i<bits<3> func, string itstr>
    : FRInstSB<func, BR_I_IT, (outs),
              (ins GPR:$rs2, GPR:$rs1, simm15:$imm15),
              itstr, "$rs2, $rs1, $imm15"> {
  let isBranch = 1;
  let isTerminator = 1;
}

// Load commands

let hasSideEffects = 0, mayLoad = 1, mayStore = 0 in
class load_i<bits<3> func, string itstr>
    : FRInstIL<func, LD_I_IR, (outs GPR:$rd), (ins GPR:$rs2, simm15:$imm15),
              itstr, "$rd, ${imm15}(${rs2})">;

// Store commands

let hasSideEffects = 0, mayLoad = 0, mayStore = 1 in
class store_i<bits<3> func, string itstr>
    : FRInstSB<func, ST_I_IR, (outs),
              (ins GPR:$rs2, GPR:$rs1, simm15:$imm15),
              itstr, "$rs2, ${imm15}(${rs1})">;

// Arithmetical and logical commands (RR)

let hasSideEffects = 0, mayLoad = 0, mayStore = 0 in
class al_rr<bits<3> func, string itstr>
    : FRInstR<func, AL_RR_IT, (outs GPR:$rd), (ins GPR:$rs2, GPR:$rs1),
              itstr, "$rd, $rs2, $rs1">;

// Arithmetical and logical commands (RI)

let hasSideEffects = 0, mayLoad = 0, mayStore = 0 in
class al_ri<bits<3> func, string itstr>
    : FRInstIL<func, AL_RI_IT, (outs GPR:$rd), (ins GPR:$rs2, simm15:$imm15),
              itstr, "$rd, $rs2, $imm15">;

//===----------------------------------------------------------------------===//
// defining instructions                                                      //
//===----------------------------------------------------------------------===//

def BNE  : branch_i<BNE_FUNC, "bne">;
def BEQ  : branch_i<BEQ_FUNC, "beq">;
def BGE  : branch_i<BGE_FUNC, "bge">;
def BLE  : branch_i<BLE_FUNC, "ble">;

def LW   : load_i<LW_FUNC, "lw">;

def SW   : store_i<SW_FUNC, "sw">;

def ADDI : al_ri<ADD_FUNCI, "addi">;
def SUBI : al_ri<SUB_FUNCI, "subi">;
def ORI  : al_ri<OR_FUNCI , "ori" >;
def ANDI : al_ri<AND_FUNCI, "andi">;
def XORI : al_ri<XOR_FUNCI, "xori">;
def SLLI : al_ri<SLL_FUNCI, "slli">;
def SRLI : al_ri<SRL_FUNCI, "srli">;
def SRAI : al_ri<SRA_FUNCI, "srai">;

def ADD  : al_rr<ADD_FUNC, "add">;
def SUB  : al_rr<SUB_FUNC, "sub">;
def OR   : al_rr<OR_FUNC , "or" >;
def AND  : al_rr<AND_FUNC, "and">;
def XOR  : al_rr<XOR_FUNC, "xor">;
def SLL  : al_rr<SLL_FUNC, "sll">;
def SRL  : al_rr<SRL_FUNC, "srl">;
def SRA  : al_rr<SRA_FUNC, "sra">;
