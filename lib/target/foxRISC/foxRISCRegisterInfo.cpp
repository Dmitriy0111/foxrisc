#include "foxRISCRegisterInfo.h"
#include "foxRISC.h"
#include "foxRISCSubtarget.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/RegisterScavenging.h"
#include "llvm/CodeGen/TargetFrameLowering.h"
#include "llvm/CodeGen/TargetInstrInfo.h"
#include "llvm/Support/ErrorHandling.h"

#define GET_REGINFO_HEADER
#include "foxRISCGenRegisterInfo.inc"

using namespace llvm;

foxRISCRegisterInfo::foxRISCRegisterInfo() : foxRISCRegisterInfo(foxRISC::X1) {}

const MCPhysReg *
foxRISCRegisterInfo::getCalleeSavedRegs(const MachineFunction *MF) const {
  // Doesn't save regs
  return CC_Save_SaveList;
}

BitVector foxRISCRegisterInfo::getReservedRegs(const MachineFunction &MF) const {
  BitVector Reserved(getNumRegs());

  markSuperRegs(Reserved, foxRISC::X0); // zero
  markSuperRegs(Reserved, foxRISC::X1); // ra
  markSuperRegs(Reserved, foxRISC::X2); // sp

  assert(checkAllSuperRegsMarked(Reserved));

  return Reserved;
}

const TargetRegisterClass*
foxRISCRegisterInfo::getPointerRegClass(const MachineFunction &MF, unsigned Kind) const {
  return &foxRISC::GPRRegClass;
}

void foxRISCRegisterInfo::eliminateFrameIndex(MachineBasicBlock::iterator II,
                                            int SPAdj, unsigned FIOperandNum,
                                            RegScavenger *RS) const {

  MachineInstr &MI = *II;
  MachineFunction &MF = *MI.getParent()->getParent();
  MachineRegisterInfo &MRI = MF.getRegInfo();
  const foxRISCInstrInfo *TII = MF.getSubtarget<foxRISCSubtarget>().getInstrInfo();
  DebugLoc DL = MI.getDebugLoc();

  int FrameIndex = MI.getOperand(FIOperandNum).getIndex();
  unsigned FrameReg;
  int Offset =
      getFrameLowering(MF)->getFrameIndexReference(MF, FrameIndex, FrameReg) +
      MI.getOperand(FIOperandNum + 1).getImm();

  if (!isInt<32>(Offset)) {
    report_fatal_error(
        "Frame offsets outside of the signed 32-bit range not supported");
  }

  MachineBasicBlock &MBB = *MI.getParent();
  bool FrameRegIsKill = false;

  if (!isInt<16>(Offset)) {
    assert(isInt<32>(Offset) && "Int32 expected");
    // The offset won't fit in an immediate, so use a scratch register instead
    // Modify Offset and FrameReg appropriately
    Register ScratchReg = MRI.createVirtualRegister(&foxRISC::GPRRegClass);
    TII->movImm(MBB, II, DL, ScratchReg, Offset);
    BuildMI(MBB, II, DL, TII->get(foxRISC::ADD), ScratchReg)
        .addReg(FrameReg)
        .addReg(ScratchReg, RegState::Kill);
    Offset = 0;
    FrameReg = ScratchReg;
    FrameRegIsKill = true;
  }

  MI.getOperand(FIOperandNum)
      .ChangeToRegister(FrameReg, false, false, FrameRegIsKill);
  MI.getOperand(FIOperandNum + 1).ChangeToImmediate(Offset);
}

Register foxRISCRegisterInfo::
getFrameRegister(const MachineFunction &MF) const {
  return foxRISC::X2;
}
