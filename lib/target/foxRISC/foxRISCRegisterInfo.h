#ifndef foxRISCREGISTERINFO_H
#define foxRISCREGISTERINFO_H

#include "llvm/CodeGen/TargetRegisterInfo.h"

#define GET_REGINFO_HEADER
#include "foxRISCGenRegisterInfo.inc"

namespace llvm {

//class TargetInstrInfo;

struct foxRISC : public foxRISCGenRegisterInfo {

  foxRISCRegisterInfo(unsigned HwMode);

  const MCPhysReg* getCalleeSavedRegs(const MachineFunction *MF = 0) const override;

  BitVector getReservedRegs(const MachineFunction &MF) const override;

  const TargetRegisterClass* getPointerRegClass(const MachineFunction &MF, unsigned Kind = 0) const override;

  void eliminateFrameIndex(MachineBasicBlock::iterator MI, int SPAdj, unsigned FIOperandNum, RegScavenger *RS = nullptr) const override;

  Register getFrameRegister(const MachineFunction &MF) const override;

};
}

#endif
