
#include "foxRISC.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/TargetRegistry.h"
using namespace llvm;

Target llvm::ThefoxRISCTarget;

extern "C" void LLVMInitializefoxRISCTargetInfo() {
  RegisterTarget<Triple::foxRISC,
        /*HasJIT=*/true> X(ThefoxRISCTarget, "foxRISC", "foxRISC");
}
